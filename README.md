# INSTALLATION #

Copy .env.dist as .env and specify variables

Once you've docker installed on your computer, run 
``` docker-compose up --build ``` at root

# COMMANDS #

- First place yourself at root of 'project', if you do 'ls', you should see manage.py

## Create project
### Create API
``` python manage.py startapp <project name> ```

## Migrations
### Create migration
``` python manage.py makemigrations <project name> ```

### Migration
``` python manage.py migrate ```

## Running commands
### Run server
``` python manage.py runserver ```

### Create account admin
``` python manage.py createsuperuser ```
or ``` docker-compose exec backend python project/manage.py createsuperuser ```
in docker

### Refresh frontend app in docker
``` docker-compose exec backend npm run dev ```

### Use Hot reloading
``` ./watch-front.sh ```