from django.db import models

class Article(models.Model):
    name = models.CharField(max_length=100, unique=True)
    content = models.CharField(max_length=100, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)