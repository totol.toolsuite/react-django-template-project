#!/bin/bash
if [ ! -f /usr/app/.env ]; then
  >&2 echo ".env file not found. Stopping container."
  docker stop back
fi
pipenv install --system
# pipenv shell
python manage.py migrate
npm install -C frontend
# change to npm run build when deploy
npm run build -C frontend

python manage.py runserver 0.0.0.0:8000